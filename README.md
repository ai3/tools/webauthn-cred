webauthn-cred
===

Small and compact command-line tool used to generate WebAuthN
registrations for external applications. Its primary use case is to
enlist hardware tokens in the file-based backend of
[id/auth](https://git.autistici.org/id/auth).

# Build

The tools uses the *libfido2* C library to talk to the hardware
devices, so you'll have to install the development packages for it:

```shell
sudo apt install build-essential libfido2-dev
```

Then it's possible to build the binary:

```shell
go build -o webauthn-cred main.go
```

Note that there seems to be no libfido2.a static library available in
Debian, so it's not possible to generate a fully static binary.

# Usage

Run the tool with the *--rpid* flag set to the WebAuthN Relying Party
ID that is used by the application you're registering for. The tool
will output the key handle and public key corresponding to the new
registration. Example:

```
$ webauthn-cred --rpid=login.example.com
autodetected device: Yubico YubiKey OTP+FIDO+CCID
touch the device (you may be asked for a pin first)......
PIN> 
key handle: "m9fihYynTqp3GQzrOsJIwID3VThXw63Je6kD8T7W734SaY2gFPSJz4NYimZZa73Qygb/toZT4FSGvpF4HbFbZQ=="
public key: "pAECAyYhWCCt7/3iAJTqFJhwcIp3qHqxC2XfhQQbShOYhu41cB7QNSJYIDeIfO/IckpJctrFBARqxDX+hk32eY0Z4KLSzjJ6B2Fv"
```

Both the key handle and public key are base64-encoded, the public key
is additionally encoded in COSE format.
