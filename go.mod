module git.autistici.org/ai3/tools/webauthn-cred

go 1.19

require (
	github.com/duo-labs/webauthn v0.0.0-20221205164246-ebaf9b74c6ec
	github.com/fxamacker/cbor/v2 v2.7.0
	github.com/keys-pub/go-libfido2 v1.5.3
	golang.org/x/term v0.25.0
)

require (
	github.com/pkg/errors v0.9.1 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf // indirect
	golang.org/x/sys v0.26.0 // indirect
)
